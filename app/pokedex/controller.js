function pokedex($scope) {
  $scope.title = "Pok3d3x"
  $scope.pokemons = pokemons
  $scope.newPokemon = {}
  $scope.addPokemon = function () {
    console.log($scope.newPokemonForm);
    $scope.pokemons.push($scope.newPokemon)
    $scope.newPokemon = {}
  }
  $scope.edit = pokemon => {
    pokemon.edit = true
  }
  $scope.update = function () {
    this.pokemon.edit = false
  }
  $scope.delete = function () {
    $scope.pokemons = $scope.pokemons.filter(function (pokemon) {
      return this.pokemon != pokemon
    }.bind(this))
    // ES2015
    // $scope.pokemons = $scope.pokemons.filter(pokemon => this.pokemon != pokemon)
  }
}
